<?php

/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 26/02/18
 * Time: 21:48
 */

use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;
use Pecee\Http\Response;
use Pecee\SimpleRouter\SimpleRouter;
use \Firebase\JWT\JWT;

class APIMiddleware implements IMiddleware {

    public function handle(Request $request) {
        // Check si le token est valide
        $token = null;
        if (!empty($_POST['token']) || !empty($_GET['token'])) {
            if(!empty($_POST['token']))
            {
                $token = $_POST['token'];
            }
            if(!empty($_GET['token']))
            {
                $token = $_GET['token'];
            }
            $key = "n_ebw/+LhJ{8w^j&h?+E*&x";
            try {
                $decoded = JWT::decode($token , $key, array('HS256'));
            } catch (Exception $e) {
                $this->redirectError($request);
            }
        } else {
            // $response = new Response($request);
            // return $response;
            $this->redirectError($request);
        }
    }

    private function redirectError ($request) {
        // $url = SimpleRouter::getUrl('login');
        // $request->setRewriteUrl($url);
        $request->setRewriteCallback('LoginController@error');
        return $request;
    }
}