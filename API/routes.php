<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:38
 */

use Pecee\SimpleRouter\SimpleRouter;
require_once 'APIMiddleware.php';
require_once __DIR__.'/models/config.php';

$prefix = CONFIG['prefix'];

// on ajoute un préfixe car le site se trouve dans mon cas à l'adresse :
// http://localhost/simplon/routeur/

SimpleRouter::group(['prefix' => $prefix], function () {
    SimpleRouter::get('/', 'DefaultController@defaultAction');
    SimpleRouter::get('/subscribers', 'SubscriberController@getAll');
    SimpleRouter::get('/meetup', 'MeetupController@getAll');
    SimpleRouter::get('/thismeetup-{id}', 'MeetupController@getOne');
    SimpleRouter::get('/delete-meetup-{id}', 'MeetupController@delete')->addMiddleware(APIMiddleware::class);
    SimpleRouter::post('/add-meetup', 'MeetupController@add')->addMiddleware(APIMiddleware::class);
    SimpleRouter::get('/login', 'LoginController@error')->name('login');
    SimpleRouter::get('/edit-meetup-{id}', 'MeetupController@getEdit')->addMiddleware(APIMiddleware::class);
    SimpleRouter::post('/confirm-edit-meetup', 'MeetupController@edit');
    SimpleRouter::post('/login', 'LoginController@login');
    SimpleRouter::post('/subscriberAdd-{id}', 'MeetupController@subscriberAdd')->addMiddleware(APIMiddleware::class);
});