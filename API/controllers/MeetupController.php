<?php

namespace Controllers;
use \Firebase\JWT\JWT;
use Models\Meetup;
use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;
use Pecee\Http\Response;
use Pecee\SimpleRouter\SimpleRouter;

class MeetupController extends Controller {
    function getAll() {
        $meetup = new Meetup();
        echo json_encode($meetup->getAll());
    }

    function getEdit($id){
        $meetup = new Meetup();
        echo json_encode($meetup->getMeetupEdit($id));
    }

    function getOne($id){
        $meetup = new Meetup();
        echo json_encode($meetup->getOneMeetup($id));
    }

    function edit(){
        $id = $_POST['id'];
        $title = $_POST['title'];
        $description = $_POST['description'];
        $location = $_POST['locationE'];
        $date = $_POST['date'];
        $organisateurs = $_POST['organisateurs'];
        $meetup = new Meetup();
        $meetup->MeetupEdit($id,$title,$description,$date,$location);
        if($organisateurs != 'null'){
        $meetup->MeetupAddSpeaker($id,$organisateurs);
        }
    }

    function delete($id){
        $meetup = new Meetup();
        $meetup->MeetupDelete($id);
        // header('Location:'.$_SERVER['HTTP_REFERER']);
    }

    function add(){
        $meetup = new Meetup();
        $title = $_POST['title'];
        $description = $_POST['description'];
        $location = $_POST['locationD'];
        $date = $_POST['date'];
        $meetup->MeetupAdd($title,$description,$date,$location);
        // header('Location:'.$_SERVER['HTTP_REFERER']);
    }

    function subscriberAdd($id_meetup){
        $token = $_POST['token'];
        $key = "n_ebw/+LhJ{8w^j&h?+E*&x";
            try {
                $decoded = JWT::decode($token , $key, array('HS256'));
                $decoded_array = (array) $decoded;
                $id = $decoded_array['id_user'];
                $meetup = new Meetup();
                $meetup->subscriberAddOnce($id,$id_meetup);
            } catch (Exception $e) {
                $this->redirectError($request);
            }
    }

    private function redirectError ($request) {
        $request->setRewriteCallback('LoginController@error');
        return $request;
    }
}