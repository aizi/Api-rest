<?php

namespace Controllers;

use Models\Subscriber;

class SubscriberController extends Controller {
    function getAll() {
        $subscriber = new Subscriber();
        echo json_encode($subscriber->getAll());
    }
}