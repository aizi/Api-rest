<?php

namespace Controllers;

class DefaultController extends Controller {
    function defaultAction() {
        $data = array('Message' => 'Welcome on my first API');
        echo json_encode($data);
    }
}