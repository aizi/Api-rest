<?php
//model par defaut avec la connexion bdd + la function pour l'appeler.
namespace Models;

class Model{
        private $db;

        function __construct(){
            $user = CONFIG['database']['user'];
            $password = CONFIG['database']['password'];
            $host = CONFIG['database']['host'];
            $database = CONFIG['database']['database'];

        // connection
        $this->db = new \PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password);
        $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }

        /*-----GETTER------*/

        function getConnection(){
            return $this->db;
        }
    }
?>