<?php

namespace Models;

class Meetup extends Model{

    function __construct(){
        parent::__construct();
    }

    function getAll(){
      $request = $this->getConnection()->query("SELECT * FROM meetup");
      return $request->fetchAll();
    }

    function getOneMeetup($id){
        $request = $this->getConnection()->query("SELECT l.address as location_address,l.id as location_id,m.id as meetup_id,m.title as meetup_title,m.description as meetup_description,m.date as meetup_date,COUNT(ms.id_meetup) as subscriber FROM `meetup` as m 
        INNER JOIN `location` as l ON m.id_location = l.id
        INNER JOIN `meetup_subscriber` as ms ON m.id = ms.id_meetup
        WHERE m.id =".$id);
        $result = $request->fetchAll(\PDO::FETCH_ASSOC);

        $requestSpeaker = $this->getConnection()->query("SELECT s.id as id_s,
        s.first_name as first_name,
        s.last_name as last_name
        FROM `speaker` as s
        INNER JOIN `meetup_speaker` as ms ON ms.id_speaker = s.id
        INNER JOIN `meetup` as m ON m.id= ms.id_meetup
        WHERE m.id =".$id);
        $result2 = $requestSpeaker->fetchAll(\PDO::FETCH_ASSOC);

        if(!empty($result2)){
            $meetup = array_merge($result,[$result2]);
            return $meetup;
        }else{
            return $result;
        }
      }

    function MeetupEdit($id,$title,$description,$date,$locationE){
        if(!empty($title) && !empty($description) && !empty($date) && !empty($locationE)){
            $locationRequest= $this->getConnection()->prepare("UPDATE `location` SET `address`=:address WHERE id=(SELECT id_location FROM meetup WHERE id=:id)");
            $locationRequest->execute([
                'address'=>$locationE,
                'id'=>$id
            ]);

            $request = $this->getConnection()->prepare("UPDATE `meetup` SET `title`=:title,`description`=:description,`date`=:date WHERE id=:id");
            $request->execute([
                'id'=>$id,
                'title'=>$title,
                'description'=>$description,
                'date'=>$date
            ]);

            $select = $this->getConnection()->query("SELECT * FROM meetup WHERE id=".$id);
            $selection = $select->fetchAll();
            echo json_encode($selection[0]);
            }else{
                echo 'La modifcation n\'a pas été pris en compte';
                http_response_code(403);
            }
    }

    function MeetupAddSpeaker($id,$organisateurs){
        if(!empty($organisateurs)){
            for($i = 0;$i<count($organisateurs);$i++){
                $organisateur = $organisateurs[$i];
                $nb = strlen($organisateur);
                $chaine = strpos($organisateur," ");
                $chaine2 = $nb - $chaine;
                $chaine2--;
                $first_name = substr($organisateur, 0, $chaine);
                $last_name = substr($organisateur, 0, -$chaine2);
            $request = $this->getConnection()->prepare("SELECT id,first_name,last_name FROM speaker WHERE first_name=:first_name or last_name=:last_name");
            $request->execute([
                'first_name'=>$first_name,
                'last_name'=>$last_name
            ]);
            $count = $request->rowCount();
            $result = $request->fetch();
            var_dump($result);
            if($count == 1){
                $id_test = $result["id"];
                $link = $this->getConnection()->prepare("INSERT INTO `meetup_speaker`(`id_meetup`, `id_speaker`) VALUES (:id_meetups,:id_speaker)");
                $link->execute([
                    "id_meetup"=> intval($id),
                    "id_speaker"=> intval($id_test)
                ]);
            }else{
                $insert = $this->getConnection()->prepare("INSERT INTO `speaker`(`first_name`, `last_name`) VALUES (:first_name,:last_name)");
                $insert->execute([
                    'first_name'=>$first_name,
                    'last_name'=>$last_name
                ]);

                $result = $request->fetch();
                $link = $this->getConnection()->prepare("INSERT INTO `meetup_speaker`(`id_meetup`, `id_speaker`) VALUES (:id_meetup,(SELECT MAX(id) FROM speaker))");
                $link->execute([
                    "id_meetup"=> $id
                ]);
            };
        }
        }else{
            echo 'La modifcation n\'a pas été pris en compte';
            http_response_code(403);
        }
    }

    function getMeetupEdit($id){
        $request = $this->getConnection()->query("SELECT m.id as meetup_id,m.title as meetup_title,m.description as meetup_description,m.date as meetup_date,l.address as meetup_address FROM `meetup` as m INNER JOIN `location` as l ON m.id_location = l.id WHERE m.id =".$id);
        $request2 = $this->getConnection()->query("SELECT s.id as speaker_id,s.first_name as speaker_first_name,s.last_name as speaker_last_name FROM `speaker` as s");
        $request3 = $this->getConnection()->query("SELECT s.id as id_s,
        s.first_name as first_name,
        s.last_name as last_name
        FROM `speaker` as s
        INNER JOIN `meetup_speaker` as ms ON ms.id_speaker = s.id
        INNER JOIN `meetup` as m ON m.id= ms.id_meetup
        WHERE m.id =".$id);
         $result = $request->fetchAll(\PDO::FETCH_ASSOC);
         $result2 = $request2->fetchAll(\PDO::FETCH_ASSOC);
         $result3 = $request3->fetchAll(\PDO::FETCH_ASSOC);
        if(!empty($result3)){
            $meetup = array_merge($result,[$result2]);
            $term = array_merge($meetup,[$result3]);
            return $term;
        }else{
            $meetup = array_merge($result,[$result2]);
            return $meetup;
        }
      }

    function MeetupDelete($id){
        $selectIDLocation =  $this->getConnection()->prepare("SELECT id_location FROM meetup WHERE id=:id");
        $selectIDLocation->execute([
            'id'=>$id
        ]);

        $resultID =  $selectIDLocation->fetch();

        $request = $this->getConnection()->prepare("DELETE FROM `meetup` WHERE id=:id");
        $request->execute([
            'id'=>$id
        ]);

        $deleteLocation = $this->getConnection()->prepare("DELETE FROM `location` WHERE id=:id");
        $deleteLocation->execute([
            'id'=>$resultID[0]
        ]);
    }

    function MeetupAdd($title,$description,$date,$locationD){
        if(!empty($title) && !empty($description) && !empty($date) && !empty($locationD)){
            $locationRequest= $this->getConnection()->prepare("INSERT INTO `location` (`address`) VALUES (:address)");
            $locationRequest->execute([
                'address'=>$locationD
            ]);

            $request = $this->getConnection()->prepare("INSERT INTO `meetup`(`title`, `description`, `date`,`id_location`) VALUES (:title,:description,:date,(SELECT MAX(`id`) FROM location))");
            $request->execute([
                'title'=>$title,
                'description'=>$description,
                'date'=>$date
            ]);


            $select = $this->getConnection()->query("SELECT MAX(`id`) FROM meetup");
            $selection = $select->fetch();
            echo json_encode($selection[0]);
        }else{
            echo 'Vous n\'avez pas pu ajouter de meetup';
            http_response_code(403);
        }
    }

    function subscriberAddOnce($id,$id_meetup){
        $check= $this->getConnection()->prepare("SELECT * FROM meetup_subscriber WHERE id_subscriber =:id AND id_meetup=:id_meetup");
        $check->execute([
            'id'=>$id,
            'id_meetup'=>$id_meetup
        ]);
        $count = $check->rowCount();
        if($count == 1){
            http_response_code(404);
            echo json_encode('Vous participer déja a ce meetup');
        }else{
        $request = $this->getConnection()->prepare("INSERT INTO meetup_subscriber (`id_meetup`,`id_subscriber`) VALUES (:id_meetup,:id)");
        $request->execute([
            "id_meetup"=>$id_meetup,
            "id"=>$id
        ]);
        echo json_encode('Vous participer désormais a cette évenement');
        }
    }
}