<?php

/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:43
 */
namespace Models;

class Subscriber {
    private $firstName;
    private $lastName;
    private $mailAddr;
    private $dateSubscription;

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getMailAddr() {
        return $this->mailAddr;
    }

    public function setMailAddr($mailAddr) {
        $this->mailAddr = $mailAddr;
    }

    public function getDateSubscription() {
        return $this->dateSubscription;
    }

    public function setDateSubscription($dateSubscription) {
        $this->dateSubscription = $dateSubscription;
    }

    public function getAll() {
        // Ici votre requête SQL (peut-être qu'un extends serait sympa pour la connexion en DB...)
        // Les données ci-dessous son fakes !
        return [
            ['first_name' => 'Jack', 'last_name' => 'Dawson', 'mail_addr' => 'jack@dawson.com', 'date_subscription' => '2018-02-25 12:15:00'],
            ['first_name' => 'Bob', 'last_name' => 'Dylan', 'mail_addr' => 'bob@dylan.com', 'date_subscription' => '2018-02-25 12:15:00'],
            ['first_name' => 'Gérard', 'last_name' => 'Depardieu', 'mail_addr' => 'gerard@depardieu.com', 'date_subscription' => '2018-02-25 12:15:00']
        ];
    }
}
