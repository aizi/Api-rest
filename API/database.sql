
---
--- MEETUP DATABASE
--- Script MYSQL
---

CREATE TABLE location (
  id bigint(20) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  address varchar(255) NOT NULL,
  city varchar(80) NOT NULL,
  zip_code varchar(5) NOT NULL,
  description text
);

CREATE TABLE subscriber (
  id bigint(20) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  first_name varchar(50) NOT NULL,
  last_name varchar(50),
  mail_addr varchar(80) NOT NULL,
  date_subscription datetime NOT NULL
);

CREATE TABLE speaker (
  id bigint(20) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  first_name varchar(50) NOT NULL,
  last_name varchar(50) NOT NULL,
  description text
);

--- À COMPLÉTER !
/*
CREATE TABLE meetup (
  id bigint(20) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  title varchar(45) NOT NULL,
  location_id bigint(20) UNSIGNED,
  ...
);

ALTER TABLE meetup ADD CONSTRAINT event_location FOREIGN KEY (id) REFERENCES location (id) ON DELETE CASCADE
..
*/