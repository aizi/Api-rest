<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:32
 */

require_once 'vendor/autoload.php';
require_once 'routes.php';

use Pecee\SimpleRouter\SimpleRouter;

SimpleRouter::setDefaultNamespace('\Controllers');

// Start the routing
SimpleRouter::start();